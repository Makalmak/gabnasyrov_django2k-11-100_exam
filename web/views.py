from django.shortcuts import render
from web.models import Item

def main_view(request):
    items = Item.objects.all()
    return render(request, 'webapp/main.html', {
                'items': items
    })
