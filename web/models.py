from django.db import models


class Item(models.Model):
    title = models.CharField(max_length=500)
    description = models.TextField(null=True)
    link = models.CharField(max_length=500)

